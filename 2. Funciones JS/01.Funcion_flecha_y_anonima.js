/*AARON QUINTANAR*/

/*original*/
function power_(base, exponente){
	let ans=base;
	for(let i=2; i<=exponente; i++)
		ans*=base;
	returns ans;
}

/*Modificado*/
function power_(base, exponente){
	let ans=base;
	for(let i=2; i<=exponente; i++)
		ans*=base;
		console.log(ans);
	return ans;
}
power_(2, 3);

/*función flecha*/
const power_ = (base, exponente) => {
	let ans=base; 
	for(let i=2; i<=exponente; i++)
		ans*=base;
		console.log(ans);
}

power_(2, 3);

/*función anónima*/
(function (base, exponente){
	let ans=base;
	for(let i=2; i<=exponente; i++)
		ans*=base;
		console.log(ans);
	return ans;
}(2, 3));